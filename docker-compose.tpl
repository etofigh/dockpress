#
# Copyright © 2019 Ehsan Tofigh
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Project: dockpress
#
# @file  docker-compose.tpl
# @brief Docker compose file for running the https proxy server.
#

version: '3.3'

services:
  server:
    image: steveltn/https-portal:1
    ports:
      - 80:80
      - 443:443
    restart: always
    volumes:
      - ./ssl_certs:/var/lib/https-portal
    networks: [ default"$networks" ]
    environment:
      STAGE: production
      DOMAINS: '"$domains"'
      CLIENT_MAX_BODY_SIZE: 64M

networks: ""$links""
