#!/bin/bash
#
# Copyright © 2019 Ehsan Tofigh
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Project: dockpress
#
# @file  dockpress.sh
# @brief Script to compose and run docker containers that serve wordpress sites.
#

set -e

# CD into the script's directory:
cd "${BASH_SOURCE%/*}/"

networks=""
links=""
domains=""

# Build the configuration for all sites:
for dir in sites/*; do
	if [ -d "${dir}" ]; then
		domain=$(basename -- "${dir}")
		service="${domain//./}"

		networks="${networks}, ${service}_default"
		links="${links}\n  ${service}_default:\n    external: true"
		domains="${domains}${domain} -> http://${service}_web_1:80, www.${domain} => https://${domain}, "

		# Bring up the wordpress website composition:
		(cd "${dir}" && docker-compose up -d)
	fi
done

# Load the template docker compose file:
template=$(<docker-compose.tpl)

# Generate the docker compose file:
compose=$(eval "echo -e \"${template}\"")

if [[ $(< docker-compose.yml) != "${compose}" ]]; then
	echo "generating docker-compose file"
	echo "${compose}" > docker-compose.yml

	# Start the docker-compose daemon:
	docker-compose up -d
else
	echo "docker-compose file up-to-date"
fi
